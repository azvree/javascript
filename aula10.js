let animais = [
    {nome: 'fluff', especie: 'coelho'},
    {nome: 'toto', especie: 'cachorro'},
    {nome: 'hamilton', especie: 'cachorro'},
    {nome: 'ursula', especie: 'gato'},
    {nome: 'harold', especie: 'peixe'},
    {nome: 'jimmy', especie: 'peixe'}
]

/*let display = 'sao amigos'

for (let i = 0; i < animais.length; i++){
    display = animais[i].nome + ' ' +display;

}
*/
let display = animais.reduce((acc, curr) => { return `${curr.nome} ${acc}`; 
}, 'são amigos')

/*let display = animais.reduce((acc, {nome}) => { return `${nome} ${acc}`; 
}, 'são amigos')*/

console.log(display)

