let animais = [
    {nome: 'fluff', especie: 'coelho'},
    {nome: 'toto', especie: 'cachorro'},
    {nome: 'hamilton', especie: 'cachorro'},
    {nome: 'ursula', especie: 'gato'},
    {nome: 'harold', especie: 'peixe'},
    {nome: 'jimmy', especie: 'peixe'}
];

let newArray = animais.map(({nome , especie}) => `${nome} é um ${especie}`);

console.log(newArray)