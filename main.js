const $ = document;

function addLista(){
    let elemPai = $.querySelector("#list-id"); // seleção do elemento pai
    //let elemPai = $.getElementById("#lista")
    //let elemPai = $.getElementsByClass("#lista")

    let elemFilho = $.createElement("li"); //criação do lemento filho

    elemFilho.innerText = "olá mundo"; // texto interno da tag alterado

    elemFilho.id = "bg-red"; // ID adicionado
    elemFilho.classList.add("bg-blue"); // classe adicionada ao elemFilho
    elemFilho.style.margin = "30px";

    elemPai.appendChild(elemFilho);
}

addLista();


let adicionar = function(){
    let pai = $.querySelector("#list-id");
    let filho = $.createElement("li");
    filho.classList.add("bt-remov");
    filho.innerText = 'Funcionou';
    pai.appendChild(filho);
}

let remover = function(){
    let pai = $.querySelector("#list-id");
    let filhos = pai.children;
    let filho = filhos[0];
    filho.remove();
}

let elemButAdd = $.querySelector(".bt-add");
let elemButRem = $.querySelector("#list-id");


elemButAdd.addEventListener('click', adicionar);

elemButRem.addEventListener('click', remover);
