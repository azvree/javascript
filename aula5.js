let someArray = [1, 2, 3];
let [primeiro, segundo, terceiro] = someArray;

console.log(terceiro);

let aluno = {
    nome: 'roberto',
    matricula: 2222222,
}

let {nome, matricula} = aluno;

let dog = {
    nome: 'bolinha',
    idade: 5,
    som: 'auau'
}


/*function fazSom(objeto) {
    let nome = objeto.nome || 'Godofredo'; 
    let som = objeto.nome || 'miau'; 
    console.log(`O ${objeto.nome} faz ${objeto.som}`)
}*/

let fazSom = ({ nome = 'godofredo', som = 'miau'}) => {
    console.log(`O ${nome} faz ${som}`)
}

fazSom(dog);
