let me = {nome: 'marcelo', sobrenome: 'nassar', idade: 22};

let amIOlder = function(){
    for(let i = 0; i < arguments.length; i++){
        if(arguments[i] < this.idade){
            console.log('sim');
        }
        else {
            console.log('nao')
        }
    }
    
    console.log(`${this.nome} ${this.sobrenome}`);
}

amIOlder.call(me, 30, 20, 50);

amIOlder.apply(me, [30, 20 , 50]);