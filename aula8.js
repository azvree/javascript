let animais = [
    {nome: 'fluff', especie: 'coelho'},
    {nome: 'toto', especie: 'cachorro'},
    {nome: 'hamilton', especie: 'cachorro'},
    {nome: 'ursula', especie: 'gato'},
    {nome: 'harold', especie: 'peixe'},
    {nome: 'jimmy', especie: 'peixe'}
]

/*let cachorro = [];

for (let i = 0; i < animais.length; i++) {    
    if (animais[i].especie === 'cachorro') {
        cachorro.push(animais[i]);
    }
}

console.log(cachorro)*/

let cachorros = animais.filter((animal) => {
    return animal.especie === 'cachorro';
})

//let cachorros = animais.filter((animal) => animal.especie === 'cachorro');

console.log(cachorros)